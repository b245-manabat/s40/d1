const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const courseController = require("../Controllers/courseController");

// Route for creating a course
router.post("/", auth.verify, courseController.addCourse);
router.get("/all", auth.verify, courseController.allCourses);
router.get("/allActive", courseController.allActiveCourses);
router.get("/allInactive", auth.verify, courseController.allInactiveCourses);
router.get("/:courseId", courseController.courseDetails);
router.put("/update/:courseId", auth.verify, courseController.updateCourse);

module.exports = router;