const Course = require("../Models/coursesSchema.js");
const auth = require("../auth.js");
const mongoose = require("mongoose");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

module.exports.addCourse = (request, response) =>{
	let input = request.body;
	const userData = auth.decode(request.headers.authorization);

		if(userData.isAdmin) {let newCourse = new Course({
			name: input.name,
			description: input.description,
			price: input.price
		});
	
		// saves the created object to our database
		return newCourse.save()
		// course succesfully created
		.then(course =>{
			console.log(course);
			response.send(course);
		})
		// course creation failed
		.catch(error =>{
			console.log(error);
			response.send(false);
		})}
		else {
			return response.send("You are not an admin.");
		}
}

module.exports.allCourses = (request,response) => {
	const userData = auth.decode(request.headers.authorization);
	if(!userData.isAdmin){
		return response.send("You are not an admin.")
	}
	else {
		Course.find({})
		.then(result=>response.send(result))
		.catch(error=>response.send(error))
	}
}

module.exports.allActiveCourses = (request,response) => {
	Course.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error));
}

module.exports.allInactiveCourses = (request,response) => {
	const userData = auth.decode(request.headers.authorization);
	if(!userData.isAdmin){
		return response.send("You are not an admin.")
	}
	else {
		Course.find({isActive: false})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	}
}

module.exports.courseDetails = (request,response) => {
	const courseId = request.params.courseId;

	Course.findById(courseId)
	.then(result => response.send(result))
	.catch(error => response.send(error));
}

module.exports.updateCourse = (request,response) => {
	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;
	const input = request.body;

	if(!userData.isAdmin) {
		return response.send("You don't have access in this page.")
	}
	else {
			Course.findById(courseId)
			.then(result => {
				if(result === null) {
					return response.send("Course ID is invalid, please try again.")
				}else{
					
					let updatedCourse = {
						name: input.name,
						description: input.description,
						price: input.price
					}
					
					Course.findByIdAndUpdate(courseId,updatedCourse,{new:true})
					.then(result => {
						console.log(result);
						return response.send(result)})
					.catch(error => response.send(error));
				}
			})
			.catch(error => response.send(error));
	}
}